#!/usr/bin/python

# JIRA commit acceptance python client for SVN
# Author: istvan.vamosi@midori.hu
# $Id$

import os
import sys
import urlparse
import httplib
import urllib
import urlparse
from os.path import normpath
from os.path import basename

# configure JIRA access
jiraBaseURL = '<JIRA base URL>'

# use this if your repos are named the same as your project keys
repoName = basename(normpath(sys.argv[1]))

# use *  to enforce using the global rule set
# or specify a project key. Might well be the same as the repoName
ruleSetKey = '*'

# configure svnlook path
svnlookPath = '<Full path to svnlook>'

# configure connection timeout
connectTimeout = 30

# Allow override feature
documentationLink = '<Documentation URL>'
propertyName = 'ALLOW_OVERRIDE'
overrideKeyword = 'OVERRIDE'

### NO CONFIGURATION BELOW THIS LINE ###

repoPath = sys.argv[1]
txnOrRevId = sys.argv[2]
if (len(sys.argv) == 3):
	revTxnArg = "--transaction"
elif (len(sys.argv) == 4):
	revTxnArg = sys.argv[3]

# get committer
try:
	f = os.popen(svnlookPath + ' author ' + repoPath + ' ' + revTxnArg + ' ' + txnOrRevId)
	committer = f.read()
	if f.close():
		raise 1
	committer = committer.rstrip("\n\r")
except:
	print >> sys.stderr, 'Unable to get committer with svnlook.'
	sys.exit(1)

# get commit message
try:
	f = os.popen(svnlookPath + ' log ' + repoPath + ' ' + revTxnArg + ' ' + txnOrRevId)
	commitMessage = f.read()
	if f.close():
		raise 1
	commitMessage = commitMessage.rstrip('\n\r')
except:
	print >> sys.stderr, 'Unable to get commit message with svnlook.'
	sys.exit(1)

# print arguments
print >> sys.stderr, 'Committer: ' + committer
print >> sys.stderr, 'Commit message: "' + commitMessage + '"'

# override feature is disabled as long as JIRA is reachable
noConnection = False

# invoke JIRA web service
try:
	params = urllib.urlencode({'committerName': committer, 'commitMessage': commitMessage, 'ruleSetKey': ruleSetKey, 'repoName': repoName})
	headers = {"Content-type": "application/x-www-form-urlencoded", "Accept": "text/json"}
	(scheme, netloc, path, baseUrlParams, query, fragment) = urlparse.urlparse(jiraBaseURL)
	conn = httplib.HTTPConnection(netloc)
	conn.connect()
	conn.sock.settimeout(connectTimeout)
	conn.request("GET", path + "/rest/com.atlassian.jira.ext.commitacceptance.server.evaluator.RestEvaluatorService/1.0/commitacceptance/cancommit.json?" + params )
	response = conn.getresponse()
	# 403: ACCESS FORBIDDEN, 408: REQUEST_TIMEOUT, 502: BAD_GATEWAY, 503: SERVICE_UNAVAILABLE, 504: GATEWAY_TIMEOUT
	if response.status in [403,408,502,503,504]:
		raise 1
	acceptance = response.status == 200
	comment = response.read()
	if response.status == 404:
		comment = "Plugin not installed"
except:
	acceptance, comment = [False, 'Unable to connect to the JIRA server at "' + jiraBaseURL + '".']
	noConnection = True

# if we don't have a connection to JIRA, check if override mode is enabled
if noConnection:
	propset = ""

	# get the list of properties for the repository root folder
	try:
		f = os.popen(svnlookPath + ' proplist ' + repoPath + ' /' )
		propset = f.read()
		if f.close():
			raise 1
	except:
		print >> sys.stderr, 'Unable to list properties with svnlook.'
		sys.exit(1)

	# check if the override allowed property is set
	if propertyName in propset:
		try:
			f = os.popen(svnlookPath + ' propget ' + repoPath + ' ' + propertyName + ' /' )
			allowoverrideprop = f.read()
			if f.close():
				raise 1
			# check if it is set to 'yes'
			if allowoverrideprop == 'yes':
				# check whether the comment starts with OVERRIDE so that this special commit becomes traceable in the commit log
				if commitMessage.startswith (overrideKeyword):
					# Override allowed, comment starts with OVERRIDE, allowing commit acceptance bypass
					acceptance = True
				else:
					comment += "\nOverride is allowed, but comment does not start with " + overrideKeyword + "."
			else:
				comment += "\nThe property " + propertyName + " is not set to 'yes', override not allowed."
		except:
			print >> sys.stderr, 'Unable to read property ' + propertyName + ' with svnlook.'
			sys.exit(1)
	else:
		comment += "\nOverride is not allowed for this repository.\n\n"
		comment += "If you want to be able to commit when JIRA is not available,\n"
		comment += "please read the documentation here: " + documentationLink

if acceptance:
	print >> sys.stderr, 'Commit accepted.'
	sys.exit(0)
else:
	print >> sys.stderr, 'Commit rejected: ' + comment
	sys.exit(1)
