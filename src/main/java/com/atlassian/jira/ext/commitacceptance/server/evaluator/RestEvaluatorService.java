package com.atlassian.jira.ext.commitacceptance.server.evaluator;

import com.atlassian.core.util.map.EasyMap;
import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.bc.issue.search.SearchService;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.ext.commitacceptance.server.action.AcceptanceSettings;
import com.atlassian.jira.ext.commitacceptance.server.action.AcceptanceSettingsManager;
import com.atlassian.jira.ext.commitacceptance.server.action.AcceptanceSettingsManagerImpl;
import com.atlassian.jira.issue.search.SearchException;
import com.atlassian.jira.jql.builder.JqlQueryBuilder;
import com.atlassian.jira.jql.parser.JqlParseException;
import com.atlassian.jira.jql.parser.JqlQueryParser;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.jira.util.JiraKeyUtils;
import com.atlassian.jira.util.json.JSONObject;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import com.atlassian.query.Query;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.text.MessageFormat;
import java.util.List;

@Path("/commitacceptance")
public class RestEvaluatorService {

    Logger log = Logger.getLogger(RestEvaluatorService.class);
    private final String GLOBAL_PROJECT_KEY = "*";

    @GET
    @AnonymousAllowed // todo: remove this
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/cancommit")
    public Response setConfigXml(
            @QueryParam("ruleSetKey") String ruleSetKey,
            @QueryParam("repoName") String repoName,
            @QueryParam("committerName") String committerName,
            @QueryParam("commitMessage") String commitMessage
    ) {

//        log.debug (ComponentAccessor.getComponent(JqlQueryParser.class));
//        log.debug(ComponentAccessor.getComponent(AcceptanceSettingsManager.class));
        ProjectManager projectManager = ComponentAccessor.getProjectManager();

        if (StringUtils.isBlank(committerName)) {
            // note: this will rarely be the authenticated user
            return Response.serverError().entity("No committer name specified").build();
        }

        if (StringUtils.isBlank(repoName)) {
            // note: this will rarely be the authenticated user
            return Response.serverError().entity("Repo name should have been passed by the hook but wasn't").build();
        }

        // todo: sort this out
        AcceptanceSettingsManagerImpl settingsManager = new AcceptanceSettingsManagerImpl(ComponentAccessor.getApplicationProperties());

        Project project = null;

        if (! GLOBAL_PROJECT_KEY.equals(ruleSetKey)) {
            project = projectManager.getProjectObjByKey(ruleSetKey);
            if (project == null) {
                return Response.serverError().entity("Project key: " + ruleSetKey + " not found").build();
            }
        }

        AcceptanceSettings settings = settingsManager.getSettings(project != null ? project.getKey() : null);
        String jqlQuery = settings.getJqlQuery();
        if (StringUtils.isBlank(jqlQuery)) {
            log.debug("OK - no jql query specified");
            return Response.ok().build();
        }

        JqlQueryParser jqlQueryParser = ComponentAccessor.getComponent(JqlQueryParser.class);
        SearchService searchService = ComponentAccessor.getComponent(SearchService.class);
        JqlQueryBuilder jqlQueryBuilder = ComponentAccessor.getComponent(JqlQueryBuilder.class);

        Query baseQuery;
        try {
            // do subsitutions in base query... might have to be done once per issue key
            jqlQuery = jqlQuery.replaceAll("\\$repoName", repoName);

            baseQuery = jqlQueryParser.parseQuery(jqlQuery);
        } catch (JqlParseException e) {
            log.warn("Bad query", e);
            return Response.serverError().entity("Bad query " + jqlQuery + "- contact administrators").build();
        }

        // todo: havent' checked committerName is a valid user yet

        Boolean anyMatched = false;
        List<String> issueKeys = JiraKeyUtils.getIssueKeysFromString(commitMessage);

        // todo: make it configurable whether any issue keys at all need to be provided
        if (issueKeys.isEmpty()) {
            return Response.serverError()
                    .entity("An issue key needs to be provided to associate with this commit.").build();
        }

        for (String issueKey : issueKeys) {
            log.debug(MessageFormat.format("Comparing {0} against {1} for committer named {2} ",  issueKey, jqlQuery, committerName));
            try {
                Query modQuery = JqlQueryBuilder.newBuilder(baseQuery).where().defaultAnd().issue().eq(issueKey).buildQuery();
                log.debug("modQuery: " + modQuery);

                long searchCount = searchService.searchCount(getUser(committerName), modQuery);
                log.debug ("Search count " + searchCount);

                if (searchCount != 1) {
                    if (settings.getAcceptIssuesFor() == AcceptanceSettings.ONLY_FOR_THIS) {
                        return getNonMatchingErrorMessage(jqlQuery);
                    }
                }
                else {
                    anyMatched = true;
                }

            } catch (SearchException e) {
                log.warn("Search error", e);
                return Response.serverError().entity("Search error").build();
            }
        }

        if (settings.getAcceptIssuesFor() == AcceptanceSettings.ONLY_FOR_THIS ||
                (settings.getAcceptIssuesFor() == AcceptanceSettings.ONE_FOR_THIS && anyMatched)) {
            return Response.ok().build();
        }
        else {
            return getNonMatchingErrorMessage(jqlQuery);
        }
    }

    private Response getNonMatchingErrorMessage(String jqlQuery) {
        return Response.serverError()
                .entity("Each issue key in the commit message needs to be present in the following query results: " +
                        jqlQuery).build();
    }

    protected User getUser(String userName) {
        UserManager userManager = ComponentAccessor.getUserManager();
        return userManager.getUser(userName);
    }
}
